/*
    https://gitlab.com/pc15020/so-gt4-pc15020/-/tree/master/Ej3_
    Autor: Juan Pleitez
    Correo: pc15020@ues.edu.sv
*/
#include <stdio.h>

void imprimir(int a){
    a = a + 1;
    printf("%d\n", a);
}

int main(){
    int i, a=0;
    for (i = 0; i < 7; i++){
        imprimir(a);
    }
    return 0;
}
