/*
    https://gitlab.com/pc15020/so-gt4-pc15020/-/tree/master/Ej7_
    Autor: Juan Pleitez
    Correo: pc15020@ues.edu.sv
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(){

    int grado;              // Grado del polinomio
    double x;               // Punto a evaluar
    double *coeficientes;   // Puntero de coeficientes

    // Obtener el grado
    printf("Ingrese el grado del polinomio: ");
    scanf("%d/n",&grado);
    printf("\n");

    // ReDimensionamos la variable coeficientes (grado + 1) porque incluimos A0 y X0
    coeficientes = malloc((grado + 1) * sizeof(double));
    for (int i = 0; i < (grado + 1); i++){
        printf("Ingrese el Coeficiente A%d: ", i);
        scanf("%lf",&coeficientes[i]);
    }
    printf("\n");
    
    // Obtener el valor de X a evaluar
    printf("Ingrese el valor de X a evaluar: ");
    scanf("%lf",&x);
    printf("\n");

    // Imprimir y Evaluar la funcion
    double resultado = 0.0;
    printf("Funcion a evaluar y resultado P(x):\nP(%3.2f) = ", x);
    for (int i = 0; i < (grado + 1); i++){
        printf("(%3.2f).(X^%d)", coeficientes[i], i);
        resultado += (coeficientes[i])*(pow(x, i));
        if(i != grado ){
            printf(" + ");
        }
    }
    printf(" = %3.2f\n", resultado);

    return 0;
}
