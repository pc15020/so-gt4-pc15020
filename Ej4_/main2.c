/*
    https://gitlab.com/pc15020/so-gt4-pc15020/-/tree/master/Ej4_
    Autor: Juan Pleitez
    Correo: pc15020@ues.edu.sv
*/
#include <stdio.h>

int main(){

    int *pnumero;
    int num1, num2;
    char *pchar;
    char letra1;
    num1 = 2;
    num2 = 5;
    letra1 = 'a';

    pnumero = &num1;

    printf("pnumero = %p\n", pnumero);
    printf("num1 = %d\n", num1);
    printf("num2 = %d\n", num2);
    printf("pchar = %p\n", pchar);
    printf("letra1 = %c\n", letra1);
    
    return 0;
}
