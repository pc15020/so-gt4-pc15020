/*
    https://gitlab.com/pc15020/so-gt4-pc15020/-/tree/master/Ej9_
    Autor: Juan Pleitez
    Correo: pc15020@ues.edu.sv
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define worldSize 10 //Es un estimado del tamanio de una palabra

int main(){
    
    int cantidad;
    printf("Ingrese la cantidad de palabras a reservar: ");
    scanf("%d",&cantidad);
    printf("\n");

    char palabras[cantidad][worldSize];

    printf("Ingrese las palabras separadas por saltos de line(ENTER):\n");
    for (int i = 0; i < cantidad; i++){
        printf("palabra #%d: ", (i+1));
        scanf("%s", palabras[i]);
    }
    printf("\n");

    /////sortText(palabras, cantidad);
    int c, d;
    char t[worldSize];
    for (c = 0 ; c < cantidad - 1; c++) {
        for (d = 0 ; d < cantidad - c - 1; d++) {
            if (strcmp(palabras[d],palabras[d+1]) > 0) {
                for (int k = 0; k < worldSize; k++){
                    t[k]   = palabras[d][k];
                }
                for (int k = 0; k < worldSize; k++){
                    palabras[d][k]   = palabras[d+1][k];
                }
                for (int k = 0; k < worldSize; k++){
                    palabras[d+1][k] = t[k];
                }
            }
        }
    }

    printf("Salida del texto ordenado:\n");
    for (int i = 0; i < cantidad; i++){
        printf("%s\n", palabras[i]);
    }
    printf("\n");

    return 0;
}
