/*
    https://gitlab.com/pc15020/so-gt4-pc15020/-/tree/master/Ej9_
    Autor: Juan Pleitez
    Correo: pc15020@ues.edu.sv
*/

#include<stdio.h>
#include<stdlib.h>

int main()
{
    int cantidad;
    char texto;
    char *ptr = &texto;

    printf("Enter number of characters to store: ");
    scanf("%d", &cantidad);

    printf("Ingrese las palabras separadas por saltos de line(ENTER):\n");
    for(int i=0; i < cantidad; i++){
        char palabra[10];
        printf("palabra #%d: ", (i+1));
        scanf("%s", &palabra);        
        ptr[i] = palabra;
        printf("%s\n", ptr);
        ptr++;
    }
    printf("\n");

    printf("Salida del texto ordenado:\n");
    for (int i = 0; i < cantidad; i++){
        printf("%s\n", ptr);
        ptr++;
    }
    printf("\n");

    // signal to operating system program ran fine
    return 0;
}