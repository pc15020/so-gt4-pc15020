/*
    https://gitlab.com/pc15020/so-gt4-pc15020/-/tree/master/Ej6_
    Autor: Juan Pleitez
    Correo: pc15020@ues.edu.sv
*/
#include <stdio.h>

int getMax(int *list, int listSize){
    int max = list[0];
    for (int i = 0; i < listSize; i++){
        if(list[i] > max){
            max = list[i];
        }
    }
    return max;
}

int getMin(int *list, int listSize){
    int min = list[0];
    for (int i = 0; i < listSize; i++){
        if(list[i] < min){
            min = list[i];
        }
    }
    return min;
}

float getAverage(int *list, int listSize){
    long total = 0;
    for (int i = 0; i < listSize; i++){
        total += list[i];
    }
    return (float) total / listSize;
}

int main(){

    int cantidad;
    printf("Cantidad de numeros a analizar: ");
    scanf("%d",&cantidad);
    
    int numeros[cantidad];
    printf("\n\n");
    printf("Ingrese cada uno de los valores numericos\n");
    for (int i = 0; i < cantidad; i++){
        scanf("%d/n",&numeros[i]);
    }
    
    printf("Numero Menor: %d\r\n", getMin(numeros, cantidad));
    printf("Numero Mayor: %d\r\n", getMax(numeros, cantidad));
    printf("Numero Promedio: %3.2f\r\n", getAverage(numeros, cantidad));
    
    return 0;
}
