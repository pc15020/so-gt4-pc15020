/*
    https://gitlab.com/pc15020/so-gt4-pc15020/-/tree/master/Ej10_
    Autor: Juan Pleitez
    Correo: pc15020@ues.edu.sv
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

typedef struct{
    char nombre[40];
    int dui;
    float sueldo;
} empleado;

int getRandomNumber(int min, int max){
    return (rand() % (max - min + 1)) + min;
}

#define cantidadEmpelados 5

int main(){

    empleado empleados[cantidadEmpelados];

    // Generar Datos Aleatorios
    printf("1-Datos Aleatorios:\n");
    for (int i = 0; i < cantidadEmpelados; i++){
        empleados[i].dui = getRandomNumber(10000000, 99999999);
        empleados[i].sueldo = (float) getRandomNumber(100, 10000);
        printf("dui = %d\t", empleados[i].dui);
        printf("sueldo = $%3.2f\t", empleados[i].sueldo);
        //Crear un nombre al azar
        char nombre[40];
        for (int i = 0; i < getRandomNumber(3, 12); i++){
            nombre[i] = (char) getRandomNumber(97, 122);
        }        
        strcpy(empleados[i].nombre, nombre);
        printf("nombre = %s\n", empleados[i].nombre);
    }
    printf("\n");

    // Imprimir Valores Ordenados por la Primera Fila Nombre
    printf("2-Ordenar e Imprimir Valores Ordenados por la Primera Fila Nombre\n");
    int c, d;
    char t[40];
    for (c = 0 ; c < cantidadEmpelados - 1; c++) {
        for (d = 0 ; d < cantidadEmpelados - c - 1; d++) {
            if (strcmp(empleados[d].nombre,empleados[d+1].nombre) > 0) {
                for (int k = 0; k < 40; k++){
                    t[k]   = empleados[d].nombre[k];
                }
                for (int k = 0; k < 40; k++){
                    empleados[d].nombre[k]   = empleados[d+1].nombre[k];
                }
                for (int k = 0; k < 40; k++){
                    empleados[d+1].nombre[k] = t[k];
                }
            }
        }
    }
    printf("\n");   
    for (int i = 0; i < cantidadEmpelados; i++){
        printf("dui = %d\t", empleados[i].dui);
        printf("sueldo = $%3.2f\t", empleados[i].sueldo);
        printf("nombre = %s\n", empleados[i].nombre);
    }
    printf("\n");

    // Imprimir Valores Ordenados por la segunda Fila Sueldo
    printf("3-Imprimir Valores Ordenados por la segunda Fila Sueldo\n");
    double tsueldo;
    for (c = 0 ; c < cantidadEmpelados - 1; c++) {
        for (d = 0 ; d < cantidadEmpelados - c - 1; d++) {
            if (empleados[d].sueldo > empleados[d+1].sueldo) {
                tsueldo   = empleados[d].sueldo;                
                empleados[d].sueldo   = empleados[d+1].sueldo;                
                empleados[d+1].sueldo = tsueldo;                
            }
        }
    }   
    for (int i = 0; i < cantidadEmpelados; i++){
        printf("dui = %d\t", empleados[i].dui);
        printf("sueldo = $%3.2f\t", empleados[i].sueldo);
        printf("nombre = %s\n", empleados[i].nombre);
    }

    return 0;
}
