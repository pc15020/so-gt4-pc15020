/*
    https://gitlab.com/pc15020/so-gt4-pc15020/-/tree/master/Ej1_
    Autor: Juan Pleitez
    Correo: pc15020@ues.edu.sv
*/
#include <stdio.h>

int main(){
    float n1, n2;
    float *p1, *p2;
    n1 = 4.0;
    p1 = &n1;
    p2 = p1;
    n2 = *p2;
    n1 = *p1 + *p2;

    printf("El valor de n1 es %3.2f\r\n", n1);
    printf("El valor de n2 es %3.2f\r\n", n2);

    return 0;
}
